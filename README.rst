========
Presence
========

Presence calculates times between machine boots and shutdowns, i.e. it gives the "uptime" over multiple days/reboots.

Print presence times by running the console script:

.. code-block:: bash

    $ presence
